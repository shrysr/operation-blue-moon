#+AUTHOR: gandalfdwite
#+EMAIL: pravarag@gmail.com
#+TAGS: dev ops read meeting
* GOALS
** DevOps
*** Learning Terraform
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: OPS.1563198652
    :TASKID: OPS.1563198652
    :END:
    - [ ] Introduction To Terraform   ( 1h )
    - [ ] Terraform CLI               ( 3h )
    - [ ] Terraform With AWS          ( 3h )
    - [ ] Terraform With Azure        ( 3h )
** Python
** Emacs
** Linux
*** TODO Linux Under the Hood [0/13]
    :PROPERTIES:
    :ESTIMATED: 24
    :ACTUAL:
    :OWNER: gandalfdwite
    :ID: READ.1573405076
    :TASKID: READ.1573405076
    :END:
    - [ ] 7. Understanding processes                     ( 2h )
    - [ ] 8. Security                                    ( 3h )
    - [ ] 9. Hardware Initilization                      ( 1h )
    - [ ] 10. Looking closer at the kernel               ( 2h )
    - [ ] 11. Networking                                 ( 1h )
    - [ ] 12. Performance optimization                   ( 1h )
    - [ ] 13. Future of Linux                            ( 1h )
** Blog - pravaragdotcom
*** Write a blog post on TravisCI
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:
    :OWNER:    gandalfdwite
    :ID:       WRITE.1576074953
    :TASKID:   WRITE.1576074953
    :END:
* PLAN
